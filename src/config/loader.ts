import * as dotenv from 'dotenv';
import { configService } from './config.service';

const dotenvPaths = ['.env', '.env.dev'];

dotenvPaths.map((path) => {
  dotenv.config({ path: process.cwd() + '/' + path });
});

module.exports = configService.getTypeOrmConfig();
