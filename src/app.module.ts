import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from './config/config.service';
import { ContributorController } from './contributor/contributor.controller';
import { ContributorService } from './contributor/contributor.service';
import { TransactionController } from './transaction/transaction.controller';
import { TransactionService } from './transaction/transaction.service';
import { GameController } from './game/game.controller';
import { GameService } from './game/game.service';
import { GameModule } from './game/game.module';
import { ContributorModule } from './contributor/contributor.module';
import { TransactionModule } from './transaction/transaction.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    GameModule,
    TransactionModule,
    ContributorModule,
  ],
  controllers: [ContributorController, TransactionController, GameController],
  providers: [ContributorService, TransactionService, GameService],
})
export class AppModule {}
