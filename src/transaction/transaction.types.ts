import { Game } from 'src/database/entity/game.entity';
import { Wager } from 'src/database/entity/wager.entity';

export class CreateTransactionRequest {
  game: Game;
  wager: Wager;
  wageAmount: number;
}
