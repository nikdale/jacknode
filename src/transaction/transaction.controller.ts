import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Transaction } from '../database/entity/transaction.entity';
import { TransactionService } from './transaction.service';

@Controller('transaction')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Get('/all')
  @ApiOperation({
    summary: 'Get all transactions',
    description: 'Get all transactions.',
  })
  async createGame(): Promise<Transaction[]> {
    return await this.transactionService.findAll();
  }
}
