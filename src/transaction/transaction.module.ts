import { Module } from '@nestjs/common';
import { TransactionController } from './transaction.controller';
import { TransactionService } from './transaction.service';
import { TransactionRepository } from '../database/repository/transaction.repository';
import { PotRepository } from '../database/repository/pot.repository';

@Module({
  imports: [],
  controllers: [TransactionController],
  providers: [TransactionService, PotRepository, TransactionRepository],
  exports: [TransactionService],
})
export class TransactionModule {}
