import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { CreateTransactionRequest } from './transaction.types';
import { TransactionRepository } from '../database/repository/transaction.repository';
import { Transaction } from '../database/entity/transaction.entity';
import { PotRepository } from '../database/repository/pot.repository';

@Injectable()
export class TransactionService {
  private readonly transactionRepository: TransactionRepository;
  private readonly potRepository: PotRepository;
  constructor(connection: Connection) {
    this.transactionRepository = connection.getCustomRepository(
      TransactionRepository,
    );
    this.potRepository = connection.getCustomRepository(PotRepository);
  }

  async addWage(
    transactionData: CreateTransactionRequest,
  ): Promise<Transaction> {
    const amountForPots =
        (transactionData.game.wagerPercentage / 100) *
        transactionData.wageAmount,
      gamePots = transactionData.game.pot;

    const transaction = await this.transactionRepository.save({
      game: transactionData.game,
      wager: transactionData.wager,
      amount: transactionData.wageAmount,
    });

    for (let index = 0; index < gamePots.length; index++) {
      const pot = gamePots[index];
      await this.potRepository.increaseAmountById(pot.id, amountForPots);
    }

    return transaction;
  }

  async findAll() {
    return this.transactionRepository.find();
  }
}
