import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1624915151973 implements MigrationInterface {
    name = 'InitialMigration1624915151973'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "public"."pot" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" uuid NOT NULL DEFAULT gen_random_uuid(), "seed_minimum" integer NOT NULL, "current_seed" integer NOT NULL DEFAULT '0', "game_id" uuid, CONSTRAINT "PK_3ea0a0ba6568626d4000b708ce5" PRIMARY KEY ("id")); COMMENT ON COLUMN "public"."pot"."created_at" IS 'Create time'; COMMENT ON COLUMN "public"."pot"."updated_at" IS 'Last update time'`);
        await queryRunner.query(`CREATE TABLE "public"."wager" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" uuid NOT NULL DEFAULT gen_random_uuid(), "public_key" character varying NOT NULL, "secret_key" character varying NOT NULL, "webhook_win_url" character varying NOT NULL, "game_id" uuid, CONSTRAINT "UQ_9236622383e9358ecbc53c839ad" UNIQUE ("public_key"), CONSTRAINT "UQ_fdb1a84bd6c659b213ade49cfe6" UNIQUE ("secret_key"), CONSTRAINT "PK_1090ac59db1aecf40868056a0c7" PRIMARY KEY ("id")); COMMENT ON COLUMN "public"."wager"."created_at" IS 'Create time'; COMMENT ON COLUMN "public"."wager"."updated_at" IS 'Last update time'`);
        await queryRunner.query(`CREATE TABLE "public"."transaction" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" uuid NOT NULL DEFAULT gen_random_uuid(), "amount" integer NOT NULL, "game_id" uuid, "wager_id" uuid, CONSTRAINT "PK_fa13c33f0837fcbdd5ab433e2d5" PRIMARY KEY ("id")); COMMENT ON COLUMN "public"."transaction"."created_at" IS 'Create time'; COMMENT ON COLUMN "public"."transaction"."updated_at" IS 'Last update time'`);
        await queryRunner.query(`CREATE TABLE "public"."game" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" uuid NOT NULL DEFAULT gen_random_uuid(), "slug" character varying NOT NULL, "name" character varying NOT NULL, "seed_minimum_amount" integer NOT NULL, "wager_percentage" integer NOT NULL DEFAULT '2', CONSTRAINT "UQ_439ad225cc3a8d8e25e6b430a4f" UNIQUE ("slug"), CONSTRAINT "PK_b0fa4e4c53f41843239f4e99ef5" PRIMARY KEY ("id")); COMMENT ON COLUMN "public"."game"."created_at" IS 'Create time'; COMMENT ON COLUMN "public"."game"."updated_at" IS 'Last update time'`);
        await queryRunner.query(`CREATE TABLE "public"."jack_pot_win" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" uuid NOT NULL DEFAULT gen_random_uuid(), "amount" integer NOT NULL, "pot_id" uuid, "wager_id" uuid, CONSTRAINT "PK_c522853a4abf407c8a1a1744e20" PRIMARY KEY ("id")); COMMENT ON COLUMN "public"."jack_pot_win"."created_at" IS 'Create time'; COMMENT ON COLUMN "public"."jack_pot_win"."updated_at" IS 'Last update time'`);
        await queryRunner.query(`ALTER TABLE "public"."pot" ADD CONSTRAINT "FK_cc744b3ea3d7525e6f786e004ee" FOREIGN KEY ("game_id") REFERENCES "public"."game"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."wager" ADD CONSTRAINT "FK_c54a8597759a174ec9ad026264e" FOREIGN KEY ("game_id") REFERENCES "public"."game"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."transaction" ADD CONSTRAINT "FK_521789c2942b1bb48e2ce364603" FOREIGN KEY ("game_id") REFERENCES "public"."game"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."transaction" ADD CONSTRAINT "FK_1747fab153df9cd00482de4e8c2" FOREIGN KEY ("wager_id") REFERENCES "public"."wager"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."jack_pot_win" ADD CONSTRAINT "FK_3f3ad80cbd87617b8861da1f5c2" FOREIGN KEY ("pot_id") REFERENCES "public"."pot"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."jack_pot_win" ADD CONSTRAINT "FK_4ec9cf439e5ff86393a0db1d5d8" FOREIGN KEY ("wager_id") REFERENCES "public"."wager"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."jack_pot_win" DROP CONSTRAINT "FK_4ec9cf439e5ff86393a0db1d5d8"`);
        await queryRunner.query(`ALTER TABLE "public"."jack_pot_win" DROP CONSTRAINT "FK_3f3ad80cbd87617b8861da1f5c2"`);
        await queryRunner.query(`ALTER TABLE "public"."transaction" DROP CONSTRAINT "FK_1747fab153df9cd00482de4e8c2"`);
        await queryRunner.query(`ALTER TABLE "public"."transaction" DROP CONSTRAINT "FK_521789c2942b1bb48e2ce364603"`);
        await queryRunner.query(`ALTER TABLE "public"."wager" DROP CONSTRAINT "FK_c54a8597759a174ec9ad026264e"`);
        await queryRunner.query(`ALTER TABLE "public"."pot" DROP CONSTRAINT "FK_cc744b3ea3d7525e6f786e004ee"`);
        await queryRunner.query(`DROP TABLE "public"."jack_pot_win"`);
        await queryRunner.query(`DROP TABLE "public"."game"`);
        await queryRunner.query(`DROP TABLE "public"."transaction"`);
        await queryRunner.query(`DROP TABLE "public"."wager"`);
        await queryRunner.query(`DROP TABLE "public"."pot"`);
    }

}
