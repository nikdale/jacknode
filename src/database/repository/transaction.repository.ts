import { Repository, EntityRepository } from 'typeorm';
import { Transaction } from '../entity/transaction.entity';

@EntityRepository(Transaction)
export class TransactionRepository extends Repository<Transaction> {
  async findById(id: string): Promise<Transaction> {
    const transaction = await this.findOne({
      relations: ['game', 'wager'],
      where: { id },
    });
    return transaction;
  }
}
