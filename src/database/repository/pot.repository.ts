import { Repository, EntityRepository } from 'typeorm';
import { Pot } from '../entity/pot.entity';

@EntityRepository(Pot)
export class PotRepository extends Repository<Pot> {
  async findById(id: string): Promise<Pot> {
    const pot = await this.findOne({
      relations: ['game'],
      where: { id },
    });
    return pot;
  }

  async increaseAmountById(id: string, increaseAmount: number): Promise<Pot> {
    const pot = await this.findOne({
      where: { id },
    });
    pot.currentSeed = pot.currentSeed + increaseAmount;
    return this.save(pot);
  }
}
