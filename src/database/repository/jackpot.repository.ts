import { Repository, EntityRepository } from 'typeorm';
import { JackPotWin } from '../entity/jackpot.entity';

@EntityRepository(JackPotWin)
export class JackPotRepository extends Repository<JackPotWin> {
  async findById(id: string): Promise<JackPotWin> {
    const jackPotWin = await this.findOne({
      relations: ['game', 'wager'],
      where: { id },
    });
    return jackPotWin;
  }
}
