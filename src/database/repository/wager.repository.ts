import { Repository, EntityRepository } from 'typeorm';
import { Wager } from '../entity/wager.entity';

@EntityRepository(Wager)
export class WageRepository extends Repository<Wager> {
  async findById(id: string): Promise<Wager> {
    const wager = await this.findOne({
      relations: ['game', 'transaction'],
      where: { id },
    });
    return wager;
  }
}
