import { Repository, EntityRepository } from 'typeorm';
import { Game } from '../entity/game.entity';

@EntityRepository(Game)
export class GameRepository extends Repository<Game> {
  async findBySlug(slug: string): Promise<Game> {
    const game = await this.findOne({
      relations: ['wager', 'pot', 'transaction'],
      where: { slug },
    });
    return game;
  }
}
