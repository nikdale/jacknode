import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BaseEntity } from './base.entity';
import { Pot } from './pot.entity';
import { Wager } from './wager.entity';

@Entity()
export class JackPotWin extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false })
  amount: number;

  @ManyToOne(() => Pot)
  @JoinColumn({
    name: 'pot_id',
    referencedColumnName: 'id',
  })
  pot: Pot;

  @ManyToOne(() => Wager)
  @JoinColumn({
    name: 'wager_id',
    referencedColumnName: 'id',
  })
  wager: Wager;
}
