import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Pot } from './pot.entity';
import { Transaction } from './transaction.entity';
import { Wager } from './wager.entity';

@Entity()
export class Game extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true, nullable: false })
  slug: string;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  seedMinimumAmount: number;

  @Column({ nullable: false, default: 2, scale: 2 })
  wagerPercentage: number;

  @OneToMany(() => Pot, (pot) => pot.game)
  pot: Pot[];

  @OneToMany(() => Transaction, (transaction) => transaction.game)
  transaction: Transaction[];

  @OneToMany(() => Wager, (wager) => wager.game)
  wager?: Wager[];
}
