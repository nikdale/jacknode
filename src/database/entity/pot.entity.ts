import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BaseEntity } from './base.entity';
import { Game } from './game.entity';

@Entity()
export class Pot extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false })
  seedMinimum: number;

  @Column({ nullable: false, type: Number, default: 0 })
  currentSeed: number;

  @ManyToOne(() => Game)
  @JoinColumn({
    name: 'game_id',
    referencedColumnName: 'id',
  })
  game: Game;
}
