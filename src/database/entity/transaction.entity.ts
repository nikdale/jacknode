import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BaseEntity } from './base.entity';
import { Game } from './game.entity';
import { Wager } from './wager.entity';

@Entity()
export class Transaction extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Game)
  @JoinColumn({
    name: 'game_id',
    referencedColumnName: 'id',
  })
  game: Game;

  @ManyToOne(() => Wager)
  @JoinColumn({
    name: 'wager_id',
    referencedColumnName: 'id',
  })
  wager: Wager;

  @Column({ nullable: false })
  amount: number;
}
