import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BaseEntity } from './base.entity';
import { Game } from './game.entity';
import { Transaction } from './transaction.entity';

@Entity()
export class Wager extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true, nullable: false })
  publicKey: string;

  @Column({ unique: true, nullable: false })
  secretKey: string;

  @Column({ unique: false, nullable: false })
  webhookWinUrl: string;

  @ManyToOne(() => Game)
  @JoinColumn({
    name: 'game_id',
    referencedColumnName: 'id',
  })
  game: Game;

  @OneToMany(() => Transaction, (transaction) => transaction.wager)
  transaction: Transaction[];
}
