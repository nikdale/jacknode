import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { utils } from '../common/index';

export class GamePots {
  @ApiProperty({
    type: Number,
    name: 'numberOfPots',
    description: 'Number of pots',
    example: 4,
    default: 4,
  })
  @IsNumber()
  @IsNotEmpty()
  numberOfPots: number;

  @ApiProperty({
    type: [Number],
    name: 'listPotsLevelAmount',
    description: `List with Seed Border Level. This is, basically, minimum amount to be seeded in POT for it to give JackPot.
       If list is shorter than number of pots required last seed number from list will be used.`,
    example: [1000, 10000, 100000, 3000000],
    default: [1000, 10000, 100000, 3000000],
  })
  @IsNumber()
  @IsNotEmpty()
  listPotsLevelAmount: number[];
}

export class CreateGameRequest {
  @ApiProperty({
    type: String,
    name: 'name',
    description: 'Game name is used for creation of a slug too..',
    example: 'Eye of HORUS',
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: Number,
    name: 'seedMinimumAmount',
    description: 'Wage amount is in EUR',
    example: 30,
    default: 10,
  })
  @IsNumber()
  @IsNotEmpty()
  seedMinimumAmount: number;

  @ApiProperty({
    type: Number,
    name: 'wagerPercentage',
    description: 'Percentage of bet to be taken for wagers',
    example: 2,
    default: 2,
    minimum: 0.1,
    maximum: 100,
  })
  @IsNumber()
  @IsNotEmpty()
  wagerPercentage: number;

  @ApiProperty({
    type: GamePots,
    name: 'gamePots',
    description: 'Game pots configuration',
  })
  @IsNotEmpty()
  gamePots: GamePots;
}

export class SetGameWinnerRequest {
  @ApiProperty({
    description: 'Provide UUID of wager you want to win.',
    example: 'd6ca525a-84d5-4e3f-a483-59bbb7ec4be1',
    required: true,
  })
  @IsUUID()
  wagerUuid: string;

  @ApiProperty({
    description: 'Provide UUID of game pot for win.',
    example: 'df300eb4-2594-448a-be44-3ec2a79815e3',
    required: true,
  })
  @IsUUID()
  potUuid: string;
}

export class GameReceiveWageRequest {
  @ApiProperty({
    type: String,
    name: 'name',
    description: 'Game name is used for creation of a slug too..',
    example: 'Eye of HORUS',
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: Number,
    name: 'seedMinimumAmount',
    description: 'Wage amount is in EUR',
    example: 30,
    default: 10,
  })
  @IsNumber()
  @IsNotEmpty()
  seedMinimumAmount: number;

  @ApiProperty({
    type: Number,
    name: 'wagerPercentage',
    description: 'Percentage of bet to be taken for wagers',
    example: 2,
    default: 2,
    minimum: 0.1,
    maximum: 100,
  })
  @IsNumber()
  @IsNotEmpty()
  wagerPercentage: number;

  @ApiProperty({
    type: GamePots,
    name: 'gamePots',
    description: 'Game pots configuration',
  })
  @IsNotEmpty()
  gamePots: GamePots;
}

export class JoinWithWage {
  @ApiProperty({
    description:
      'Wager ID is a UUID of wager. Can be used later to add more money to a game.',
  })
  wagerId: string;

  @ApiProperty({
    description:
      'Wager secret is for a wager to be only source of truth. Wager can use this secret to collect WIN if he WINS! Other part of a secret for collecting a win will be provided via registered webhook',
  })
  wagerSecret: string;
}

export class JoinWithWageResponse extends utils.GenericResponse<JoinWithWage> {}
