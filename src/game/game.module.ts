import { Module } from '@nestjs/common';
import { GameController } from './game.controller';
import { GameService } from './game.service';
import { GameRepository } from '../database/repository/game.repository';
import { PotRepository } from '../database/repository/pot.repository';
import { WageRepository } from '../database/repository/wager.repository';

@Module({
  imports: [],
  controllers: [GameController],
  providers: [GameService, PotRepository, GameRepository, WageRepository],
  exports: [GameService],
})
export class GameModule {}
