import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { utils } from '../common/index';
import { CreateGameRequest } from './game.types';
import { Game } from '../database/entity/game.entity';
import { GameRepository } from '../database/repository/game.repository';
import { PotRepository } from '../database/repository/pot.repository';
import { Pot } from '../database/entity/pot.entity';
import { Wager } from '../database/entity/wager.entity';
import { JackPotRepository } from '../database/repository/jackpot.repository';
import { WageRepository } from '../database/repository/wager.repository';

@Injectable()
export class GameService {
  private readonly gameRepository: GameRepository;
  private readonly potRepository: PotRepository;
  private readonly jackPotRepository: JackPotRepository;
  private readonly wageRepository: WageRepository;

  constructor(connection: Connection) {
    this.gameRepository = connection.getCustomRepository(GameRepository);
    this.potRepository = connection.getCustomRepository(PotRepository);
    this.jackPotRepository = connection.getCustomRepository(JackPotRepository);
    this.wageRepository = connection.getCustomRepository(WageRepository);
  }

  private async throwIfDuplicate({ slug }) {
    const dictionary = await this.gameRepository.findOne({
      where: [{ slug: slug }],
    });

    if (dictionary) {
      throw new Error('SLUG_ALREADY_EXISTS');
    }
  }

  private async postWebhook(
    urlAddress: string,
    winnerId: string,
    amount: number,
    publicKey: string,
    secretKey: string,
  ) {
    const url = new URL(urlAddress);
    const response = await fetch(url.toString(), {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'F-Origin': 'CoolPlace',
      },
      method: 'POST',
      body: JSON.stringify({
        winner: winnerId,
        amount,
        publicKey,
        secretKey,
      }),
    });
    const content = await response.json();
    return content;
  }

  async createGame(gameDto: CreateGameRequest): Promise<Game> {
    const slug = utils.slugify(gameDto.name);

    await this.throwIfDuplicate({ slug });

    const gameRecordToSave = {
      slug: slug,
      name: gameDto.name,
      seedMinimumAmount: gameDto.seedMinimumAmount,
      wagerPercentage: gameDto.wagerPercentage,
    };

    const game = await this.gameRepository.save(gameRecordToSave);

    // If not provided for any reason default will be 4
    const numberOfPots = gameDto.gamePots?.numberOfPots || 4;
    const listPotsLevelAmount = gameDto.gamePots.listPotsLevelAmount;
    const potsToSave = [];

    for (let index = 0; index < numberOfPots; index++) {
      // If not provided for any reason default will be 100
      const potSeedLvl = listPotsLevelAmount[index] || 100;
      const newPot = {
        seedMinimum: potSeedLvl,
        game,
      };
      potsToSave.push(newPot);
    }

    await this.potRepository.save(potsToSave);

    return game;
  }

  async findGameBySlug(slug: string): Promise<Game> {
    return this.gameRepository.findBySlug(slug);
  }

  async findAll(): Promise<Game[]> {
    return this.gameRepository.find();
  }

  async winnerRandomizer(pot: Pot): Promise<void> {
    if (pot.currentSeed >= pot.seedMinimum) {
      const potTransactions = pot.game.transaction;
      // 10% chance if win
      if (Math.random() < 0.1) {
        const randomTransactionFromPot =
          potTransactions[Math.floor(Math.random() * potTransactions.length)];
        const winnerWager = randomTransactionFromPot.wager;
        await this.gamePotWinnerCreate(pot, winnerWager);
      }
    }
  }

  async manuallyGamePotWinnerCreate(
    potUuid: string,
    wagerUuid: string,
  ): Promise<void> {
    console.log(`SETTING MANUALLY WINNER ${wagerUuid} FOR POT ${potUuid} !`);
    const pot = await this.potRepository.findOne(potUuid);
    if (!pot) {
      throw new Error('NO_POT_FOUND');
    }

    const winner = await this.wageRepository.findById(wagerUuid);
    if (!winner) {
      throw new Error('NO_WAGER_FOUND');
    }
    const transactions = pot.game.transaction;

    let wagerWasPartOfGame = false;

    for (let index = 0; index < transactions.length; index++) {
      const transaction = transactions[index];
      if ((transaction.wager.id = winner.id)) {
        wagerWasPartOfGame = true;
        break;
      }
    }

    if (!wagerWasPartOfGame) {
      throw new Error('PROVIDED_WAGER_WAS_NOT_PART_OF_GAME');
    }

    await this.gamePotWinnerCreate(pot, winner);
  }

  async gamePotWinnerCreate(pot: Pot, winner: Wager) {
    console.log(`WINNER FOR POT ${pot.id} IS ${winner.id}`);
    const potUpdated = pot;
    potUpdated.currentSeed = 0;
    await this.potRepository.save(potUpdated);
    await this.jackPotRepository.save({
      amount: pot.currentSeed,
      game: pot.game,
      wager: winner,
    });
    await this.postWebhook(
      winner.webhookWinUrl,
      winner.id,
      pot.currentSeed,
      winner.publicKey,
      winner.secretKey,
    );
  }
}
