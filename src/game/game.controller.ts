import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Game } from '../database/entity/game.entity';
import { GameService } from './game.service';
import { CreateGameRequest, SetGameWinnerRequest } from './game.types';

@Controller('game')
export class GameController {
  constructor(private readonly gameService: GameService) {}

  @Post('/create')
  @ApiOperation({
    summary: 'Create GAME',
    description: 'Crate an game with configuration.',
  })
  async createGame(@Body() gameDto: CreateGameRequest): Promise<Game> {
    return await this.gameService.createGame(gameDto);
  }

  @Get('/all')
  @ApiOperation({
    summary: 'Get all games',
    description: "Get all games with pots and transaction and all it's data",
  })
  async getAllGames(): Promise<Game[]> {
    return await this.gameService.findAll();
  }

  @Get('/:slug')
  @ApiOperation({
    summary: 'Get game by slug',
    description: "Get game by slug with pots and transaction and all it's data",
  })
  async getGameBySlug(@Param('slug') slug: string): Promise<Game> {
    return await this.gameService.findGameBySlug(slug);
  }

  @Post('/set-winner')
  @ApiOperation({
    summary: 'Set game winner',
    description: "Set game winner with pot UUID and wager's UUID.",
  })
  async setGameWinner(@Body() gameDto: SetGameWinnerRequest): Promise<any> {
    return await this.gameService.manuallyGamePotWinnerCreate(
      gameDto.potUuid,
      gameDto.wagerUuid,
    );
  }
}
