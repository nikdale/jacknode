import { ApiProperty } from '@nestjs/swagger';

export class ResponseError {
  @ApiProperty()
  message: string;

  @ApiProperty()
  code?: number;

  @ApiProperty()
  name?: string;

  @ApiProperty()
  payload?: undefined | Record<string, string>;
}

export class GenericResponse<T = null> {
  @ApiProperty({
    example: 200,
    type: 'number',
  })
  code = 200;

  @ApiProperty()
  success: boolean;

  @ApiProperty({
    example: [],
  })
  errors: ResponseError[] = [];

  @ApiProperty()
  data: T = null;

  constructor(data: T = null) {
    this.data = data;
  }
}
