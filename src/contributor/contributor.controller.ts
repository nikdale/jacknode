import { Body, Controller, Post } from '@nestjs/common';
import { ContributorService } from './contributor.service';
import { ApiOperation } from '@nestjs/swagger';
import { JoinWithWageRequest, JoinWithWageResponse } from './contributor.types';

@Controller('contributor')
export class ContributorController {
  constructor(private readonly contributorService: ContributorService) {}

  @Post('/join')
  @ApiOperation({
    summary: 'Join game',
    description: 'Join game by contributing.',
  })
  async joinContribute(
    @Body() data: JoinWithWageRequest,
  ): Promise<JoinWithWageResponse> {
    return this.contributorService.joinContribute(data);
  }
}
