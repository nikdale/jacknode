import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { JoinWithWageRequest, JoinWithWageResponse } from './contributor.types';
import { utils } from '../common/index';
import { Wager } from '../database/entity/wager.entity';
import { WageRepository } from '../database/repository/wager.repository';
import { GameService } from '../game/game.service';
import { TransactionService } from '../transaction/transaction.service';

@Injectable()
export class ContributorService {
  private readonly wageRepository: WageRepository;
  constructor(
    connection: Connection,
    private readonly gameService: GameService,
    private readonly transactionService: TransactionService,
  ) {
    this.wageRepository = connection.getCustomRepository(WageRepository);
  }

  async createNewContributor(webhookWinUrl: string): Promise<Wager> {
    return this.wageRepository.save({
      publicKey: utils.generateString(12),
      secretKey: utils.generateString(18),
      webhookWinUrl,
    });
  }

  async joinContribute(
    data: JoinWithWageRequest,
  ): Promise<JoinWithWageResponse> {
    // Check if UUID is provided. If yes, we should check it.
    // Done in a bad way, but for demo only.
    let contributor = undefined;
    if (data.toBeneficiaryId) {
      contributor = await this.wageRepository.findOne({
        where: { id: data.toBeneficiaryId },
      });
      if (!contributor) {
        throw new Error('PROVIDED_UUID_NOT_IN_DB');
      }
    } else {
      if (!data.webHookWinUrl) {
        throw new Error('WEBHOOK_URL_MUST_BE_PROVIDED');
      }
      contributor = await this.createNewContributor(data.webHookWinUrl);
    }
    const gameBySlug = await this.gameService.findGameBySlug(data.gameSlug);
    if (!gameBySlug) {
      throw new Error('NO_GAME_WITH_PROVIDED_SLUG');
    }

    await this.transactionService.addWage({
      game: gameBySlug,
      wager: contributor,
      wageAmount: data.wageAmount,
    });

    return {
      code: 200,
      success: true,
      errors: [],
      data: {
        wagerId: contributor.id,
        wagerSecret: contributor.secretKey,
      },
    };
  }
}
