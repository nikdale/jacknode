import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, IsUUID } from 'class-validator';
import { utils } from '../common/index';

export class JoinWithWageRequest {
  @ApiProperty({
    type: String,
    name: 'gameSlug',
    description:
      'Game SLUG for joining specifig game. If game does not exists with some slug, it will be created.',
    example: 'eye_of_horus',
  })
  @IsString()
  @IsNotEmpty()
  gameSlug: string;

  @ApiProperty({
    type: Number,
    name: 'wageAmount',
    description: 'Wage amount is in EUR',
    example: 3000,
  })
  @IsString()
  @IsNotEmpty()
  wageAmount: number;

  @ApiProperty({
    type: String,
    name: 'webHookWinUrl',
    description:
      'Provide webhook URL where you will be notified in case of win. Used only when new Wager (contributor) is created.',
    example: 'https://mywebhook.funbetgames.rs/webhooks/winwinwin',
    required: false,
  })
  @IsString()
  @IsNotEmpty()
  webHookWinUrl?: string;

  @ApiProperty({
    description: 'Provide UUID if you are adding money to existing wage.',
    example: 'd6ca525a-84d5-4e3f-a483-59bbb7ec4be1',
    required: false,
  })
  @IsUUID()
  @IsOptional()
  toBeneficiaryId?: string;
}

export class JoinWithWage {
  @ApiProperty({
    description:
      'Wager ID is a UUID of wager. Can be used later to add more money to a game.',
  })
  wagerId: string;

  @ApiProperty({
    description:
      'Wager secret is for a wager to be only source of truth. Wager can use this secret to collect WIN if he WINS! Other part of a secret for collecting a win will be provided via registered webhook',
  })
  wagerSecret: string;
}

export class JoinWithWageResponse extends utils.GenericResponse<JoinWithWage> {}
