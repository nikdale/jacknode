import { Module } from '@nestjs/common';
import { ContributorController } from './contributor.controller';
import { ContributorService } from './contributor.service';
import { WageRepository } from '../database/repository/wager.repository';
import { GameModule } from '../game/game.module';
import { TransactionModule } from '../transaction/transaction.module';

@Module({
  imports: [GameModule, TransactionModule],
  controllers: [ContributorController],
  providers: [ContributorService, WageRepository],
  exports: [ContributorService],
})
export class ContributorModule {}
