Start Docker image with PostgreSQL with:

`docker-compose -f database.yml up -d`

Bild the app:

`yarn build`

Migrate DB with:

`yarn typeorm:run`

Start APP with:

`yarn start:dev`

Swagger will be @  http://localhost:3000/api/


NOTE: Because of the issues with migrations I did not handle them as expected. Please do `yarn typeorm:drop` and `yarn typeorm:run` after `yarn build` !